let key = CryptoJS.enc.Utf8.parse('1234567890123456');

function encrypt(msgString, key) {
    let iv  = CryptoJS.enc.Utf8.parse('jm8lgqa3j1d0ajus');
    let encrypted = CryptoJS.AES.encrypt(msgString, key, {
        iv: iv
    });
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
}

function decrypt(ciphertextStr, key) {
    let ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);

    let iv = ciphertext.clone();
    iv.sigBytes = 16;
    iv.clamp();
    ciphertext.words.splice(0, 4);
    ciphertext.sigBytes -= 16;

    let decrypted = CryptoJS.AES.decrypt({ciphertext: ciphertext}, key, {
        iv: iv
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
}

function print_results(data) {
    let block = document.getElementById('label');

    for (let key in data) {
        let newDiv = document.createElement('div');
        newDiv.classList.add('results-div');

        let newPic = document.createElement('img');
        newPic.src = "data:image/png;base64," + data[key];
        newPic.width = 300;
        newPic.height = 300;
        newPic.alt = '';

        let newLabel = document.createElement('span');
        newLabel.innerText = key;

        newDiv.appendChild(newPic);
        newDiv.appendChild(newLabel);

        block.parentNode.insertBefore(newDiv, block.nextSibling);
    }
}

// var encrypted = encrypt("1234567", key);
// var decrypted = decrypt(encrypted, key);
//
// console.log(encrypted);
// console.log(decrypted);

const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

async function Main() {
    const file = document.querySelector('#video_file').files[0];
    const base64video = await toBase64(file);

    const chunkSize = 1000000;
    const numChunks = Math.ceil(base64video.length / chunkSize);
    console.log(base64video.length);
    localStorage.setItem('current_file', file.name);

    for (let i = 0; i < numChunks; i++)
    {
        let flag = true;
        let start = i*chunkSize;
        let end = Math.min(start + chunkSize, base64video.length);
        let chunk = encrypt(base64video.slice(start, end), key);
        // console.log("chunk: ", chunk);
        // console.log("chunk_num: ", i+1);
        // console.log("total: ", numChunks);
        // console.log("file_name: ", file.name);
        // console.log("file_size: ", file.size);

        let httpRequest = new XMLHttpRequest();

        httpRequest.onreadystatechange = function()
        {
            if(httpRequest.readyState === 4 && httpRequest.status !== 200) {
                alert('error');
                flag = false;
            }

            if(httpRequest.readyState === 4 && httpRequest.status === 200 && i+1 === numChunks) {
                print_results(JSON.parse(decrypt(httpRequest.responseText, key)));
            }
        }

        httpRequest.open('POST', 'http://localhost:1010/upload2', false);
        httpRequest.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
        httpRequest.send(JSON.stringify({
            "chunk": chunk,
            "chunk_num": i+1,
            "total": numChunks,
            "file_name": file.name,
            "file_size": file.size
        }));
        console.log(`${i+1}/${numChunks}`);
        if (!flag) {
            break;
        }
    }

    // window.open("results.html");
    // window.focus();
}

const file = document.querySelector('#video_file');
file.addEventListener("change", Main, false);

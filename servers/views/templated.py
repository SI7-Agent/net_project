#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import logging

import flask_cors
from flask import Blueprint, request, make_response
from werkzeug.utils import secure_filename
from config import config
from aes import *
from image_tools import *
import requests


blueprint = Blueprint('templated', __name__, template_folder='templates')
log = logging.getLogger('pydrop')


@blueprint.route('/upload2', methods=['POST'])
@flask_cors.cross_origin()
def upload2():
    data = request.json

    save_path = os.path.join(config.data_dir, secure_filename(data["file_name"]))
    save_path_metadata = os.path.join(config.data_dir, "metadata" + secure_filename(data["file_name"]))
    current_chunk = int(data["chunk_num"])

    if os.path.exists(save_path) and current_chunk == 1:
        return make_response(('File already exists', 400))

    try:
        with open(save_path_metadata, 'a') as f:
            chunk_to_write = decrypt(data["chunk"].encode("ASCII"), key)
            if current_chunk == 1:
                f.write(chunk_to_write[22:])
            else:
                f.write(chunk_to_write)
    except OSError:
        log.exception('Could not write to file')
        return make_response(("Not sure why,"
                              " but we couldn't write the file to disk", 500))

    total_chunks = int(data['total'])

    if current_chunk == total_chunks:
        with open(save_path_metadata, 'r') as f2:
            data_to_convert = f2.read()
            with open(save_path, 'wb') as fh:
                fh.write(base64.b64decode(data_to_convert))

        if os.path.getsize(save_path) != int(data['file_size']):
            log.error(f"File {data['file_name']} was completed, "
                      f"but has a size mismatch."
                      f"Was {os.path.getsize(save_path)} but we"
                      f" expected {data['file_size']} ")
            return make_response(('Size mismatch', 500))
        else:
            os.remove(save_path_metadata)
            log.info(f'File {data["file_name"]} has been uploaded successfully')
            r = requests.get('http://MeSSivO2:1010/'+data["file_name"])
            return make_response((encrypt(r.text, key).decode("ASCII"), 200))
    else:
        log.debug(f'Chunk {current_chunk + 1} of {total_chunks} '
                  f'for file {data["file_name"]} complete')

    return make_response(("OK", 200))

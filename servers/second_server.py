import socket
import signal
import sys
import time
import threading

from modules.concrete_system import MySystem
from image_tools import *
import json


processor = MySystem()


def handle_file(filename):
    path = "C:/Users/Asus/PycharmProjects/test_flask_chunk/data/"

    my_frame = cv2.VideoCapture(path + filename)
    i = 0
    objects = {}
    for c in processor.CLASSES:
        objects[c] = ''

    while my_frame.isOpened():
        ret, frame = my_frame.read()
        if ret == True:
            if i % 105 == 0:

                subpeop = processor.find_persons(frame)
                subobj = processor.find_objects(frame)

                try:
                    if len(subpeop) and objects["Person"] == '':
                        objects["Person"] = convert_imgarray_to_img64(subpeop[0]['small'])

                    for n in subobj:
                        if objects[n['label']] == '':
                            objects[n['label']] = convert_imgarray_to_img64(
                                processor.get_object_from_image([n["top"], n["right"], n["bottom"], n["left"]], frame))
                except:
                    continue

            i += 1
        else:
            break

    my_frame.release()
    cv2.destroyAllWindows()

    to_json = {}
    for i in objects.keys():
        print(i, objects[i])
        if objects[i] != '':
            to_json[i] = objects[i]

    to_json = str.encode(json.dumps(to_json))
    return to_json


class WebServer(object):
    def __init__(self, port=9999):
        self.host = socket.gethostname().split('.')[0]
        self.port = port

    def start(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            print("Starting server on {host}:{port}".format(host=self.host, port=self.port))
            self.socket.bind((self.host, self.port))
            print("Server started on port {port}.".format(port=self.port))

        except Exception as e:
            print("Error: Could not bind to port {port}".format(port=self.port))
            self.shutdown()
            sys.exit(1)

        self._listen()

    def shutdown(self):
        try:
            print("Shutting down server")
            self.socket.shutdown(socket.SHUT_RDWR)

        except Exception as e:
            pass

    def generate_headers(self, response_code):
        header = ''
        if response_code == 200:
            header += 'HTTP/1.1 200 OK\n'
        elif response_code == 404:
            header += 'HTTP/1.1 404 Not Found\n'

        time_now = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())
        header += 'Date: {now}\n'.format(now=time_now)
        header += 'Server: Simple-Python-Server\n'
        header += 'Connection: close\n\n'
        return header

    def _listen(self):
        self.socket.listen(5)
        while True:
            (client, address) = self.socket.accept()
            client.settimeout(60)
            print("Received connection from {addr}\n".format(addr=address))
            threading.Thread(target=self._handle_client, args=(client, address)).start()

    def _handle_client(self, client, address):
        PACKET_SIZE = 200000
        while True:
            data = client.recv(PACKET_SIZE).decode()

            if not data:
                break

            request_method = data.split(' ')[0]

            if request_method == "GET" or request_method == "HEAD":
                file_requested = data.split(' ')[1]
                file_requested = file_requested.split('?')[0][1:]
                print(file_requested)

                response_data = handle_file(file_requested)
                response_header = self.generate_headers(200)

                response = response_header.encode()
                response += response_data

                print(response_data)
                client.send(response)
                client.close()
                break
            else:
                print("Unknown HTTP request method: {method}".format(method=request_method))


def shutdownserver(sig, unused):
    server.shutdown()
    sys.exit(1)


signal.signal(signal.SIGINT, shutdownserver)
server = WebServer(1010)
server.start()
print("Press Ctrl+C to shut down server.")
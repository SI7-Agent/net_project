from abc import abstractmethod

import cv2
import keras.models
import numpy as np
import os


class Robot:
    control_tool = None
    known_face_encodings = None
    known_face_metadata = None
    CLASSES = None
    CLASSES_RU = None
    COLORS = None
    EMOTES = None
    EMOTES_RU = None
    model = None
    net = None
    gender_net = None
    camera = None
    builder = None

    def __init__(self):
        self.known_face_encodings = []
        self.known_face_metadata = []

        self.CLASSES = ["Background", "Aeroplane", "Bicycle", "Bird", "Boat",
                        "Bottle", "Bus", "Car", "Cat", "Chair", "Cow", "Dinning table",
                        "Dog", "Horse", "Motorbike", "Person", "Potted plant", "Sheep",
                        "Sofa", "Train", "Tv monitor"]

        self.COLORS = np.random.uniform(0, 255, size=(len(self.CLASSES), 3))

        self.EMOTES = ["Angry", "Disgust", "Scared", "Happy", "Sad", "Surprised", "Neutral"]

        self.net = cv2.dnn.readNetFromCaffe(os.getcwd() + '\\models\\prt.txt', os.getcwd() + '\\models\\mdl.caffemodel')
        self.model = keras.models.load_model(os.getcwd() + '\\models\\emt_mdl.hdf5')
        self.gender_net = cv2.dnn.readNetFromCaffe(os.getcwd() + '\\models\\gender.prototxt', os.getcwd() + '\\models\\gender.caffemodel')

    def refresh_models(self):
        self.net = cv2.dnn.readNetFromCaffe(os.getcwd() + '\\models\\prt.txt', os.getcwd() + '\\models\\mdl.caffemodel')
        self.model = keras.models.load_model(os.getcwd() + '\\models\\emt_mdl.hdf5')
        self.gender_net = cv2.dnn.readNetFromCaffe(os.getcwd() + '\\models\\gender.prototxt', os.getcwd() + '\\models\\gender.caffemodel')

    @staticmethod
    def get_face_from_image(face_location, small_frame):
        top, right, bottom, left = face_location
        face_image = small_frame[top:bottom, left:right]
        face_image = cv2.resize(face_image, (150, 150))

        return face_image

    @staticmethod
    def get_object_from_image(object_location, frame):
        top, right, bottom, left = object_location
        #
        # small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

        # object_image = small_frame[int(top / 4):int(bottom / 4), int(left / 4):int(right / 4)]
        # object_image = cv2.resize(object_image, (150, 150))

        object_image = frame[int(top):int(bottom), int(left):int(right)]
        #object_image = cv2.resize(object_image, (150, 150))

        return object_image

    @abstractmethod
    def find_gender(self, frame):
        pass

    @abstractmethod
    def find_emote(self, frame):
        pass

    @abstractmethod
    def find_persons(self, frame):
        pass

    @abstractmethod
    def find_objects(self, frame):
        pass

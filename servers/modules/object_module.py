from modules.robot import Robot

import cv2
import numpy as np


class ObjectModule(Robot):
    @staticmethod
    def obj_decorate(function):
        def find_objects(self, frame):
            predictions = []

            (h, w) = frame.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)
            self.net.setInput(blob)
            detections = self.net.forward()
            for i in np.arange(0, detections.shape[2]):
                confidence = detections[0, 0, i, 2]

                if confidence > 0.2:
                    idx = int(detections[0, 0, i, 1])
                    if self.CLASSES[idx] == "Person":
                        continue

                    submassive = {"left": 0, "top": 0, "right": 0, "bottom": 0, "label": "", "color": None}
                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")

                    label = self.CLASSES[idx]

                    submassive["label"] = label
                    submassive["left"] = startX
                    submassive["top"] = startY
                    submassive["right"] = endX
                    submassive["bottom"] = endY
                    submassive["color"] = self.COLORS[idx]
                    predictions.append(submassive)

            return predictions

        return find_objects

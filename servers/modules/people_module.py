from modules.robot import Robot

import cv2
import face_recognition


class PeopleModule(Robot):
    @staticmethod
    def people_decorate(function):
        def find_persons(self, frame):
            predictions = []

            small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

            face_locations = face_recognition.face_locations(small_frame)

            for (top, right, bottom, left) in face_locations:
                submassive = {"left": None, "top": None, "right": None, "bottom": None, "label": "", "color": None, "small": None}

                top *= 4
                right *= 4
                bottom *= 4
                left *= 4

                submassive["left"] = left
                submassive["top"] = top
                submassive["right"] = right
                submassive["bottom"] = bottom
                submassive["color"] = self.COLORS[self.CLASSES.index("Person")]
                submassive["small"] = small_frame
                predictions.append(submassive)

            return predictions

        return find_persons
